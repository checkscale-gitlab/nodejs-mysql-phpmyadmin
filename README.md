Fullstack javascript, mysql and phpmyadmin on docker, project default with express and vuejs

- Frontend with Vue and webpack
- Backend with express and nodemon

<br>

copy files:

- .dockerignore
- docker-compose.yml
- api/Dockerfile
- web/Dockerfile
